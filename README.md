# Open Mobility Indicators – iteration 0

## Installation

Some dependencies like pyrosm or osmnx are more easily installable with `conda` than with `pip`.

Using `conda` is without modifying your user environment is possible with `pyenv`.

Prerequisites: [pyenv](https://github.com/pyenv/pyenv)

```bash
# Install miniconda3-latest Python version with pyenv:
pyenv install miniconda3-latest

# Create and activate a virtualenv named test-conda-1 from the miniconda3-latest Python miniconda3-latest:
pyenv virtualenv miniconda3-latest test-conda-1
pyenv activate test-conda-1

# Your shell should be prepended by `(test-conda-1)`.

# The `conda` command is now in the PATH of your shell.

# Configure "conda-forge" external source:
conda config --prepend channels conda-forge

# Create a new conda environment named "iteration-0":
conda create -n iteration-0 --strict-channel-priority

# This "iteration-0" conda env sits in the "test-conda-1" virtualenv.

# Source the conda.sh file (once during the shell session) to avoid failing at the next step:
# (OR use `conda init bash` to append a config block to your shell config file)
. ~/.pyenv/versions/miniconda3-latest/etc/profile.d/conda.sh

# Your shell should be prepended by `(iteration-0)` and `(test-conda-1)`.

# Activate the "iteration-0" conda environment:
conda activate iteration-0

# Install project dependencies:
conda env update --file environment.yml

# environment.yml contains the dependencies of the project (deep and frozen).

# To install other packages:
conda install <package_name>
# Then regenerate environment.yml (if the added package is intended to stay):
conda env export > environment.yml
```

Then run jupyter lab:

```bash
jupyter lab
```

When you're done working with the project, you may deactivate the virtualenvs like this:

```bash
# To deactivate the conda env:
conda deactivate

# To deactivate the test-conda-1 virtualenv:
pyenv deactivate
```

## Download data

```bash
# Dependencies
apt install osmctools

bash download_data.sh
```

## Website

To launch a local dev HTTP server:

```bash
npx sirv-cli --dev website
```
