#!/bin/bash

set -e

SCRIPT_DIR=$(cd `dirname $0` && pwd)

docker run --rm \
  -p 8000:8000 \
  -v $SCRIPT_DIR/data:/tilesets \
  consbio/mbtileserver
