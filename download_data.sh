#!/bin/bash

set -e

DATA_DIR="data"

mkdir -p $DATA_DIR
pushd $DATA_DIR

# From https://www.data.gouv.fr/fr/datasets/donnees-carroyees-a-200m-sur-la-population/
if [ ! -e carroyage_insee_metro.csv ]; then
    wget -c 'https://www.data.gouv.fr/fr/datasets/r/6072929c-ba60-4a15-b797-ee3c1b20e21b' -O carroyage_insee_metro.zip
    unzip carroyage_insee_metro.zip
    rm carroyage_insee_metro.zip
fi

# Cf https://www.openstreetmap.org/relation/74272
wget -c 'http://polygons.openstreetmap.fr/get_poly.py?id=74272&params=0' -O aubagne.poly

wget -c 'https://download.openstreetmap.fr/extracts/europe/france/provence_alpes_cote_d_azur/bouches_du_rhone.osm.pbf'

osmconvert bouches_du_rhone.osm.pbf -B=aubagne.poly --complete-ways -o=aubagne.pbf

# Aix
# wget -c 'http://polygons.openstreetmap.fr/get_poly.py?id=1686391&params=0' -O aix.poly
# osmconvert bouches_du_rhone.osm.pbf -B=aix.poly --complete-ways -o=aix.pbf

# Paris
# wget -c 'http://polygons.openstreetmap.fr/get_poly.py?id=71525&params=0' -O paris.poly
# wget -c 'http://download.openstreetmap.fr/extracts/europe/france/ile_de_france-latest.osm.pbf'
# osmconvert ile_de_france-latest.osm.pbf -B=paris.poly --complete-ways -o=paris.pbf

popd