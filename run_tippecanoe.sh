#!/bin/bash

set -e

SCRIPT_DIR=$(cd `dirname $0` && pwd)

TILE_NAME="$1"

docker run -it --rm \
  -v $SCRIPT_DIR/data:/data \
  metacollin/tippecanoe:latest \
  tippecanoe --output /data/${TILE_NAME}.mbtiles /data/${TILE_NAME}.geojson $@